const setResponseHandler = require('configurapi-handler-http').setResponseHandler;

module.exports = function(event, statusCode = 200, body = '', headers = undefined) 
{
        setResponseHandler.apply({continue:()=>{}}, [event, statusCode, body, headers]);

        if(!event.response.headers)
        {
                event.response.headers = {};
        }

        if(!('Content-Type' in event.response.headers))
        {
                event.response.headers['Content-Type'] = 'application/json';
        }
        
        this.continue();
};