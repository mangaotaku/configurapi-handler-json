const Ajv = require('ajv');
const fs = require('fs');
const ErrorResponse = require('configurapi').ErrorResponse;

function validate(schemaText, data, callback)
{
    let isValid = false;
    let error = undefined;
    let errorDetails = undefined;

    try
    {
        let validator = new Ajv();
        let schema = JSON.parse(schemaText);
        isValid = validator.validate(schema, data);

        if(isValid === false)
        {
            error = validator.errorsText();
            errorDetails = validator.errors;
        }
    }
    catch(e)
    { 
        error = e;
    }

    callback(error, errorDetails, isValid);
}

function validateEmptyPayload(event)
{
    if(event.request.payload === undefined || event.request.payload === '')
    {
        return true;
    }
    else
    {
        event.response = new ErrorResponse('Request payload should be empty.', 400);
        return false;
    }
}

function validatePayload(event)
{
    return new Promise((resolve, reject) => {
        
        let schemaFile = `schemas/${event.name}.json`;

        fs.readFile(schemaFile, 'utf8', (err, schemaText) => {
            if(err)
            {
                event.response = new ErrorResponse(`Schema file '${schemaFile}' does not exist.`, 500, err)
                this.complete();
            }
            else
            {
                validate(schemaText, event.request.payload, (error, errorDetails, isValid) =>
                {
                    if(isValid)
                    {
                        this.continue();
                    }
                    else
                    {
                        event.response = new ErrorResponse(error || 'Bad request', 400, errorDetails);
                        this.complete();
                    }
                });
            }
            resolve();
        });
    });
}

module.exports = function(event, mode) 
{
    if(mode === 'emptyPayloadAccepted')
    {
        if(event.request.payload === undefined || event.request.payload === '')
        {
            return this.continue();
        }
        else
        {
            return validatePayload.apply(this, [event]);
        }
    }
    else if(mode === 'emptyPayload')
    {
        if(validateEmptyPayload(event))
        {
            this.continue();
        }
        else
        {
            this.complete();
        }
    }
    else
    {
        return validatePayload.apply(this, [event]);
    }
};