const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const validateJsonRequestHandler = require('../src/validateJsonRequestHandler');

describe('validateJsonRequestHandler', function() {
    it("Validate a valid empty payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = undefined;

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayload']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate an invalid empty payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayload']);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - with a valid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName'};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - with an invalid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload =  {name:3.14};

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate a valid optional payload - without payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();

        await validateJsonRequestHandler.apply(context, [ev, 'emptyPayloadAccepted']);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });


    it("Validate a valid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:'aName'};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isTrue(continueSpy.calledOnce);
        assert.isFalse(completeSpy.calledOnce);
        assert.notInstanceOf(ev.response, Configurapi.ErrorResponse);
    });

    it("Validate an invalid payload", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'post_object';
        ev.response = new Configurapi.Response();
        ev.request = new Configurapi.Request();
        ev.request.payload = {name:3.14};

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
        assert.equal('data.name should be string', ev.response.message);
        assert.equal(1, ev.response.details.length);
    });

    it("Schema file not found", async function()
    {    
        let continueSpy = sinon.spy();
        let completeSpy = sinon.spy();
        let context = {continue:continueSpy,complete:completeSpy};

        let ev = sinon.mock(Configurapi.Event);
        ev.name = 'schema_not_exist_event';
        ev.response = new Configurapi.Response();

        await validateJsonRequestHandler.apply(context, [ev]);

        assert.isFalse(continueSpy.calledOnce);
        assert.isTrue(completeSpy.calledOnce);
        assert.instanceOf(ev.response, Configurapi.ErrorResponse);
    });
});