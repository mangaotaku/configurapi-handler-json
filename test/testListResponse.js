const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const ListResponse = require('../src/ListResponse');

describe('ListResponse', function() {
    it("Serialize object properly", async function()
    {    
        let response = new ListResponse(['b', 'c', 'd'], {start:1,extra:false}, 202, {'X-Header': 'value'});
        
        assert.equal(1, response.body.start);
        assert.equal(3, response.body.total); 
        assert.equal(false, response.body.extra);
        assert.equal('b', response.body.items[0]);
        assert.equal('c', response.body.items[1]);
        assert.equal('d', response.body.items[2]);
        assert.equal(202, response.statusCode);
        assert.equal('application/json', response.headers['Content-Type']);
        assert.equal('value', response.headers['X-Header']);
    });

    it("Reject non-array", async function()
    {   
        let error = undefined;

        try
        {
            new ListResponse("hi");
        }
        catch(e)
        {
            error = e;
        }

        assert.isDefined(error);
        assert.equal(error.statusCode, 500);
        assert.equal(error.message, 'An array is expcted for ListResponse.');
    });

    it("Accept undefined", async function()
    {    
        let response = new ListResponse(undefined);

        assert.equal(0, response.body.total);
    });
});