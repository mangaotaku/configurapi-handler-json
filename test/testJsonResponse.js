const assert = require('chai').assert;
const sinon = require('sinon');
const Configurapi = require('configurapi');

const JsonResponse = require('../src/JsonResponse');

describe('JsonResponse', function() {
    it("Serialize object properly", async function()
    {    
        let response = new JsonResponse({hi:'there'}, 202, {'X-Header': 'value'});
        
        assert.equal('there', response.body.hi);
        assert.equal(202, response.statusCode);
        assert.equal('application/json', response.headers['Content-Type']);
        assert.equal('value', response.headers['X-Header']);
    });

    it("Keep content-type if exists", async function()
    {    
        let response = new JsonResponse({}, 200, {'Content-Type': 'text/xml charset=utf-8'});
        
        assert.equal('text/xml charset=utf-8', response.headers['Content-Type']);
    });
});